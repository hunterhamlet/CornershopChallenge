package com.cornershop.counterstest.base

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel

open class BaseApplicationViewModel(private  val app: Application): AndroidViewModel(app), ILogBase  {

    //logs functions
    override fun logDebug(message: String) {
        Log.d("JHMM", message)
    }

    override fun logInfo(message: String) {
        Log.i("JHMM", message)
    }

    override fun logWarning(message: String) {
        Log.w("JHMM", message)
    }

    override fun logError(message: String) {
        Log.e("JHMM", message)
    }

}
package com.cornershop.counterstest.base

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

open class BaseFragment : Fragment(), ILogBase, IViewBase {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onBackPress()
    }

    //livedata helper
    fun <T> LiveData<T>.observer(observer: (t: T) -> Unit) {
        this.observe(viewLifecycleOwner, Observer {
            it?.let(observer)
        })
    }

    //onBackPress setup
    open fun onBackPressFunction() {  }

    private fun onBackPress() {
        requireActivity().onBackPressedDispatcher
            .addCallback(viewLifecycleOwner, object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    onBackPressFunction()
                }
            })
    }

    //toast functions
    override fun toast(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun toastLong(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    //logs functions
    override fun logDebug(message: String) {
        Log.d("JHMM", message)
    }

    override fun logInfo(message: String) {
        Log.i("JHMM", message)
    }

    override fun logWarning(message: String) {
        Log.w("JHMM", message)
    }

    override fun logError(message: String) {
        Log.e("JHMM", message)
    }

}
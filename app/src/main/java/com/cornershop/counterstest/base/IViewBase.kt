package com.cornershop.counterstest.base

interface IViewBase {
    fun toast(message: String)
    fun toastLong(message: String)
}
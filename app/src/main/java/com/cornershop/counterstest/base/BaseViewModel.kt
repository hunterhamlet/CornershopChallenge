package com.cornershop.counterstest.base

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel(), ILogBase {

    //logs functions
    override fun logDebug(message: String) {
        Log.d("JHMM", message)
    }

    override fun logInfo(message: String) {
        Log.i("JHMM", message)
    }

    override fun logWarning(message: String) {
        Log.w("JHMM", message)
    }

    override fun logError(message: String) {
        Log.e("JHMM", message)
    }

}
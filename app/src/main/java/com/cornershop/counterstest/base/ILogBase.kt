package com.cornershop.counterstest.base

interface ILogBase {
    fun logDebug(message: String){}
    fun logInfo(message: String)
    fun logWarning(message: String)
    fun logError(message: String)
}
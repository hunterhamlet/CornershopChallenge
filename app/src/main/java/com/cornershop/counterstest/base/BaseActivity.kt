package com.cornershop.counterstest.base

import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer

open class BaseActivity : AppCompatActivity(), ILogBase, IViewBase {

    //livedata helper
    fun <T> LiveData<T>.observer(observer: (t: T) -> Unit) {
        this.observe(this@BaseActivity, Observer {
            it?.let(observer)
        })
    }

    //toast functions
    override fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun toastLong(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    //logs functions
    override fun logDebug(message: String) {
        Log.d("JHMM", message)
    }

    override fun logInfo(message: String) {
        Log.i("JHMM", message)
    }

    override fun logWarning(message: String) {
        Log.w("JHMM", message)
    }

    override fun logError(message: String) {
        Log.e("JHMM", message)
    }

}
package com.cornershop.counterstest.sealed

import com.cornershop.counterstest.util.TypeOperation
import com.cornershop.provider.models.CounterResponse

sealed class UIStates<out T : Any> {
    object Init : UIStates<Nothing>()
    object Loading : UIStates<Nothing>()
    object NoContent : UIStates<Nothing>()
    object NoContentSearch : UIStates<Nothing>()
    data class DeleteCounters(
        val deleteCounterList: MutableList<CounterResponse>
    ) : UIStates<Nothing>()

    data class Error(val message: String) : UIStates<Nothing>()
    data class HasContent<T : Any>(val data: T) : UIStates<T>()
    data class ShareContent(val counterListJson: String) : UIStates<Nothing>()
    data class ShowDeleteWarning(var counter: CounterResponse) : UIStates<Nothing>()
    object ShowDeleteListWarning : UIStates<Nothing>()
    object ShowNetworkDeleteError : UIStates<Nothing>()
    data class ShowErrorNetworkCounterOperation(val counter: CounterResponse, val typeOperation: TypeOperation) : UIStates<Nothing>()
    object ShowNetworkCreateError : UIStates<Nothing>()
}

package com.cornershop.counterstest.util

enum class TypeOperation {
    INCREMENT, DECREMENT
}
package com.cornershop.counterstest.util

import com.cornershop.provider.models.CounterResponse

typealias FunReturnCounter = (counter : CounterResponse) -> Unit
typealias FunReturnString = (string : String) -> Unit
typealias FunOnlyInvoke = () -> Unit
package com.cornershop.counterstest

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.cornershop.counterstest.di.DIModules

class Application: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        DIModules.init(this)
    }

}
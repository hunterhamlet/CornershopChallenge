package com.cornershop.counterstest.views.activity

import android.os.Bundle
import androidx.activity.viewModels
import com.cornershop.counterstest.base.BaseActivity
import com.cornershop.counterstest.databinding.ActivityCreateBinding
import com.cornershop.counterstest.databinding.ActivityMainBinding
import com.cornershop.counterstest.extensions.showErrorSnackBar
import com.cornershop.counterstest.extensions.showSuccessSnackBar
import com.cornershop.counterstest.extensions.visibility
import com.cornershop.counterstest.viewmodel.GenericMainViewModel

class CreateActivity: BaseActivity() {

    private val viewModel: GenericMainViewModel by viewModels()
    private val binding: ActivityCreateBinding by lazy {
        ActivityCreateBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupObservable()
    }

    private fun setupObservable(){
        viewModel.apply {
            loadingState.observer { binding.progressBar.visibility(it) }
            showError.observer { if (it.isNotEmpty()) binding.root.showErrorSnackBar(it) }
            showSuccess.observer { if (it.isNotEmpty()) binding.root.showSuccessSnackBar(it) }
            onBackPress.observer { if (it) finish()}
        }
    }

}
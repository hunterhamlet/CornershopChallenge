package com.cornershop.counterstest.views.fragment

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.annotation.RequiresApi
import androidx.fragment.app.activityViewModels
import androidx.navigation.navGraphViewModels
import com.cornershop.counterstest.R
import com.cornershop.counterstest.base.BaseFragment
import com.cornershop.counterstest.databinding.FragmentMainBinding
import com.cornershop.counterstest.extensions.addImeiAction
import com.cornershop.counterstest.extensions.addTextWatcherTimer
import com.cornershop.counterstest.extensions.blockUI
import com.cornershop.counterstest.extensions.createAlertDialog
import com.cornershop.counterstest.extensions.disable
import com.cornershop.counterstest.extensions.disableChangedAlpha
import com.cornershop.counterstest.extensions.enable
import com.cornershop.counterstest.extensions.enableChangedAlpha
import com.cornershop.counterstest.extensions.hide
import com.cornershop.counterstest.extensions.hideKeyboard
import com.cornershop.counterstest.extensions.show
import com.cornershop.counterstest.extensions.unblockUI
import com.cornershop.counterstest.sealed.UIStates
import com.cornershop.counterstest.util.COUNTER_LIST_SHARE
import com.cornershop.counterstest.util.TypeOperation
import com.cornershop.counterstest.viewmodel.GenericMainViewModel
import com.cornershop.counterstest.viewmodel.MainViewModel
import com.cornershop.counterstest.viewmodel.ViewModelFactory
import com.cornershop.counterstest.views.activity.CreateActivity
import com.cornershop.counterstest.views.viewholder.ProductViewHolder
import com.cornershop.provider.models.CounterResponse
import com.xwray.groupie.GroupieAdapter
import org.koin.android.ext.android.inject

class MainFragment : BaseFragment() {

    private val genericViewModel: GenericMainViewModel by activityViewModels()
    private val factoryViewModel: ViewModelFactory by inject()
    private val mainViewModel: MainViewModel by navGraphViewModels(R.id.nav_main) { factoryViewModel }
    private val binding: FragmentMainBinding by lazy {
        FragmentMainBinding.inflate(layoutInflater, null, false)
    }
    private val adapter: GroupieAdapter by lazy { GroupieAdapter() }

    override fun onStart() {
        super.onStart()
        mainViewModel.getCounters()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        genericViewModel.showLoading()
        setupSwipe()
        setupRecycler()
        setupListeners()
        setupObservables()
    }

    override fun onBackPressFunction() {
        super.onBackPressFunction()
        requireActivity().finish()
    }

    private fun setupListeners() {
        binding.apply {
            swipeRefresh.setOnRefreshListener { updateDataInProgress() }
            buttonAddCounters.setOnClickListener {
                startActivity(Intent(requireActivity(), CreateActivity::class.java))
            }
            searchBar.inputSearch.apply {
                addTextWatcherTimer { query -> searchCounterByQuery(query) }
                addImeiAction { _, actionId, _ -> addImeiAction(actionId) }
                setOnFocusChangeListener { _, focus -> emptySearch(focus) }
            }
            deleteBar.apply {
                closeDelete.setOnClickListener {
                    mainViewModel.resetSelectedList()
                    mainViewModel.getCounters()
                    hideDeleteBar()
                }
                deleteCounter.setOnClickListener {
                    mainViewModel.deleteItemWarning()
                }
                shareCounter.setOnClickListener {
                    mainViewModel.shareCounterList()
                }
            }
        }
    }

    private fun emptySearch(isFocus: Boolean) {
        if (isFocus) {
            showEmptySearch()
        }
    }

    private fun showEmptySearch() {
        binding.apply {
            overlayView.root.show()
            buttonAddCounters.disableChangedAlpha()
        }
    }

    private fun searchCounterByQuery(query: String) {
        mainViewModel.searchCounters(query)
    }

    private fun addImeiAction(actionId: Int): Boolean {
        return when (actionId) {
            EditorInfo.IME_ACTION_SEARCH -> {
                hideKeyboard()
                searchFinished()
                true
            }
            else -> false
        }
    }

    private fun searchFinished() {
        binding.apply {
            overlayView.root.hide()
            searchBar.inputSearch.clearFocus()
            buttonAddCounters.enableChangedAlpha()
        }
    }

    private fun setupObservables() {
        mainViewModel.apply {
            uiObservableState.observer { handleState(it) }
        }
    }

    private fun handleState(state: UIStates<MutableList<CounterResponse>>) {
        when (state) {
            is UIStates.NoContent -> showNoContent()
            is UIStates.HasContent -> showContent(state.data)
            is UIStates.NoContentSearch -> showNotResult()
            is UIStates.DeleteCounters -> updateUIDeleteBar(state.deleteCounterList)
            is UIStates.ShareContent -> shareContent(state.counterListJson)
            is UIStates.Loading -> showLoading()
            is UIStates.ShowErrorNetworkCounterOperation -> showNetworkErrorCounterOperation(
                state.counter,
                state.typeOperation
            )
            is UIStates.ShowDeleteWarning -> showDeleteWarning(counter = state.counter)
            is UIStates.ShowDeleteListWarning -> showDeleteListWarning()
            is UIStates.ShowNetworkDeleteError -> showNetworkDeleteError()
        }
    }

    private fun showLoading(){
        genericViewModel.showLoading()
    }

    private fun shareContent(counterListJson: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(COUNTER_LIST_SHARE, counterListJson)
            type = "text/plain"
        }
        val shareIntent = Intent.createChooser(sendIntent, getString(R.string.share))
        startActivity(shareIntent)
        mainViewModel.resetState()
    }

    private fun updateUIDeleteBar(deleteList: MutableList<CounterResponse>) {
        if (deleteList.isNotEmpty()) {
            showDeleteBar(deleteList)
        } else {
            hideDeleteBar()
        }
        mainViewModel.resetState()
    }

    private fun hideDeleteBar() {
        binding.apply {
            deleteBar.root.hide()
            searchBar.apply {
                inputSearch.show()
                root.show()
            }
        }
    }

    private fun showDeleteBar(deleteList: MutableList<CounterResponse>) {
        binding.apply {
            deleteBar.root.show()
            searchBar.apply {
                inputSearch.disable()
                root.disable()
            }
            deleteBar.counterDeleteItems.text = getString(R.string.n_selected, deleteList.size)
        }
    }

    private fun showContent(listCounter: MutableList<CounterResponse>) {
        setData(listCounter)
        genericViewModel.hideLoading()
        mainViewModel.resetState()
    }

    private fun showNotResult() {
        binding.apply {
            listProducts.hide()
            layoutItemCounters.root.hide()
            searchNotResult.show()
            overlayView.root.hide()
            searchFinished()
            unblockUI()
        }
        mainViewModel.resetState()
    }

    private fun setData(listCounter: MutableList<CounterResponse>) {
        val listCounterMap = listCounter.map { counter ->
            ProductViewHolder(counter,
                { mainViewModel.incrementCounter(it) },
                { mainViewModel.decrementCounter(it) },
                { mainViewModel.addCounterInDeleteList(counter) },
                { mainViewModel.removeCounterInDeleteList(counter) })
        }
        adapter.apply {
            clear()
            addAll(listCounterMap)
        }
        updateCounterUI(listCounter)
        updatedDataFinished()
    }

    private fun showNoContent() {
        genericViewModel.hideLoading()
        showEmptyState()
        mainViewModel.resetState()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupSwipe() {
        binding.swipeRefresh.setColorSchemeColors(requireContext().getColor(R.color.orange))
    }

    private fun setupRecycler() {
        binding.listProducts.adapter = this.adapter
    }

    private fun updateDataInProgress() {
        mainViewModel.getCounters()
        hideEmptyState()
        adapter.clear()
        binding.searchBar.inputSearch.text = null
        blockUI()
    }

    private fun updatedDataFinished() {
        binding.apply {
            layoutItemCounters.root.show()
            searchBar.inputSearch.enable()
            swipeRefresh.isRefreshing = false
            searchBar.inputSearch.clearFocus()
            listProducts.show()
            deleteBar.root.hide()
            searchNotResult.hide()
            emptyState.root.hide()
        }
        unblockUI()
    }

    private fun showEmptyState() {
        binding.apply {
            swipeRefresh.isRefreshing = false
            searchBar.inputSearch.disable()
            listProducts.hide()
            searchNotResult.hide()
            emptyState.root.show()
            unblockUI()
        }
    }

    private fun hideEmptyState() {
        binding.apply {
            searchBar.inputSearch.disable()
            emptyState.root.hide()
        }
    }

    private fun updateCounterUI(counterList: MutableList<CounterResponse>) {
        binding.apply {
            if (counterList.isNotEmpty()) {
                layoutItemCounters.categoriesCounter.text =
                    getString(R.string.n_items, counterList.size)
                var timesCounter = 0
                counterList.forEach { counter -> timesCounter += counter.count }
                layoutItemCounters.itemsCounter.text = getString(R.string.n_times, timesCounter)
            } else {
                layoutItemCounters.categoriesCounter.text =
                    getString(R.string.n_items, 0)
                layoutItemCounters.itemsCounter.text = getString(R.string.n_times, 0)
            }
        }
    }

    private fun showNetworkErrorCounterOperation(
        counter: CounterResponse,
        typeOperation: TypeOperation
    ) {
        genericViewModel.hideLoading()
        val counterValue =
            if (typeOperation == TypeOperation.INCREMENT) counter.count + 1 else counter.count - 1
        createAlertDialog(
            title = getString(
                R.string.error_updating_counter_title,
                counter.title,
                counterValue
            ),
            message = getString(R.string.connection_error_description),
            negativeString = getString(R.string.retry),
            positiveString = getString(R.string.dismiss),
            negativeFun = { mainViewModel.selectCounterOperation(counter, typeOperation) },
        ).show()
    }

    private fun showDeleteWarning(counter: CounterResponse) {
        genericViewModel.hideLoading()
        createAlertDialog(
            title = getString(R.string.delete_x_question, counter.title),
            negativeString = getString(R.string.cancel),
            positiveString = getString(R.string.delete),
            positiveFun = { mainViewModel.deleteItems() }
        ).show()
    }

    private fun showDeleteListWarning() {
        genericViewModel.hideLoading()
        createAlertDialog(
            title = getString(R.string.delete_list),
            negativeString = getString(R.string.cancel),
            positiveString = getString(R.string.delete),
            positiveFun = { mainViewModel.deleteItems() }
        ).show()
    }

    private fun showNetworkDeleteError() {
        genericViewModel.hideLoading()
        createAlertDialog(
            title = getString(R.string.error_deleting_counter_title),
            message = getString(R.string.connection_error_description),
            positiveString = getString(R.string.ok)
        ).show()
    }

}
package com.cornershop.counterstest.views.viewholder

import android.content.res.ColorStateList
import android.view.View
import com.cornershop.counterstest.R
import com.cornershop.counterstest.databinding.ItemProductViewHolderBinding
import com.cornershop.counterstest.extensions.disable
import com.cornershop.counterstest.extensions.enable
import com.cornershop.counterstest.extensions.getColor
import com.cornershop.counterstest.extensions.hide
import com.cornershop.counterstest.extensions.show
import com.cornershop.counterstest.extensions.toastLong
import com.cornershop.counterstest.util.FunReturnCounter
import com.cornershop.provider.models.CounterResponse
import com.xwray.groupie.viewbinding.BindableItem

class ProductViewHolder(
    private val counter: CounterResponse,
    private val incrementCounter: FunReturnCounter,
    private val decrementCounter: FunReturnCounter,
    private val addSelectedList: FunReturnCounter,
    private val removeOfSelectedList: FunReturnCounter
) :
    BindableItem<ItemProductViewHolderBinding>() {

    private var counterItem: Int = counter.count
    private var deleteCounterFlag: Boolean = false

    override fun bind(viewBinding: ItemProductViewHolderBinding, position: Int) {
        viewBinding.apply {
            tvProductName.text = counter.title
            counterButtons.tvCounterProduct.text = counter.count.toString()
            selectedView.alpha = 0.0f
            checked.hide()
            counterButtons.root.show()
            checkMinusState(binding = viewBinding)
            setOnCLickListener(binding = viewBinding, counter = counter)
        }
    }

    override fun getLayout(): Int = R.layout.item_product_view_holder

    override fun initializeViewBinding(view: View): ItemProductViewHolderBinding =
        ItemProductViewHolderBinding.bind(view)

    private fun checkMinusState(binding: ItemProductViewHolderBinding) {
        binding.apply {
            if (counterItem == 0) {
                counterButtons.ivMinus.apply {
                    disable()
                    imageTintList = ColorStateList.valueOf(getColor(R.color.gray))
                }
            } else {
                counterButtons.ivMinus.apply {
                    enable()
                    imageTintList = ColorStateList.valueOf(getColor(R.color.orange))
                }
            }
        }
    }

    private fun setOnCLickListener(
        binding: ItemProductViewHolderBinding,
        counter: CounterResponse
    ) {
        binding.apply {
            counterButtons.containerMinus.setOnClickListener {
                if (counterItem != 0) {
                    decrementCounter(this)
                    decrementCounter.invoke(counter)
                }
            }
            counterButtons.containerPlus.setOnClickListener {
                incrementCounter(this)
                incrementCounter.invoke(counter)
            }
            tvProductName.setOnLongClickListener {
                addOrRemoveListener(counter)
                showDeleteView(this)
                false
            }
        }
    }

    private fun addOrRemoveListener(counter: CounterResponse) {
        deleteCounterFlag = deleteCounterFlag.not()
        checkIfAddOrRemoveDeleteList(counter)
    }

    private fun checkIfAddOrRemoveDeleteList(counter: CounterResponse) {
        if (deleteCounterFlag) {
            addSelectedList.invoke(counter)
        } else {
            removeOfSelectedList.invoke(counter)
        }
    }

    private fun showDeleteView(binding: ItemProductViewHolderBinding) {
        if (deleteCounterFlag) {
            binding.apply {
                counterButtons.root.hide()
                checked.show()
                selectedView.alpha = 0.2f
            }
        } else {
            binding.apply {
                counterButtons.root.show()
                checked.hide()
                selectedView.alpha = 0.0f
            }
        }
    }

    private fun updateCounter(binding: ItemProductViewHolderBinding, count: Int) {
        binding.counterButtons.tvCounterProduct.text = count.toString()
    }

    private fun decrementCounter(binding: ItemProductViewHolderBinding) {
        if (counterItem != 0) {
            updateCounter(binding, counterItem)
        }
        checkMinusState(binding)
    }

    private fun incrementCounter(binding: ItemProductViewHolderBinding) {
        updateCounter(binding, counterItem)
    }

}
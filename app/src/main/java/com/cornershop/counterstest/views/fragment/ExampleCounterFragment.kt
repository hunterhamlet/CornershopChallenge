package com.cornershop.counterstest.views.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.cornershop.counterstest.R
import com.cornershop.counterstest.base.BaseFragment
import com.cornershop.counterstest.databinding.FragmentExampleCounterBinding
import com.cornershop.counterstest.extensions.blockUI
import com.cornershop.counterstest.extensions.createAlertDialog
import com.cornershop.counterstest.extensions.showErrorSnackBar
import com.cornershop.counterstest.extensions.showSuccessSnackBar
import com.cornershop.counterstest.extensions.unblockUI
import com.cornershop.counterstest.sealed.UIStates
import com.cornershop.counterstest.viewmodel.CreateCounterFlowViewModel
import com.cornershop.counterstest.viewmodel.ViewModelFactory
import com.cornershop.counterstest.views.viewholder.ExampleSectionViewHolder
import com.cornershop.provider.models.CounterResponse
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter
import org.koin.android.ext.android.inject

class ExampleCounterFragment : BaseFragment() {

    private val binding: FragmentExampleCounterBinding by lazy {
        FragmentExampleCounterBinding.inflate(layoutInflater, null, false)
    }
    private val factoryViewModel: ViewModelFactory by inject()
    private val createViewModel: CreateCounterFlowViewModel by navGraphViewModels(R.id.nav_create) { factoryViewModel }
    private val adapter: GroupieAdapter by lazy { GroupieAdapter() }
    private val drinkList by lazy { resources.getStringArray(R.array.drinks_array) }
    private val foodList by lazy { resources.getStringArray(R.array.food_array) }
    private val miscList by lazy { resources.getStringArray(R.array.misc_array) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecycler()
        addSections()
        setupOnClickListener()
        setupObservable()
    }

    override fun onDetach() {
        super.onDetach()
        createViewModel.resetState()
    }

    override fun onBackPressFunction() {
        super.onBackPressFunction()
        findNavController().navigateUp()
    }

    private fun setupOnClickListener() {
        binding.apply {
            ivBack.setOnClickListener { findNavController().navigateUp() }
        }
    }

    private fun setupObservable() {
        createViewModel.uiObservableState.observer { state -> handleUIState(state) }
    }

    private fun handleUIState(state: UIStates<MutableList<CounterResponse>>) {
        when (state) {
            is UIStates.Loading -> showLoading()
            is UIStates.HasContent -> hasContent()
            is UIStates.Error -> showError(state.message)
            is UIStates.ShowNetworkCreateError -> showNetworkCreateError()
        }
    }

    private fun hasContent() {
        binding.apply {
            unblockUI()
            progressSave.hide()
            root.showSuccessSnackBar(getString(R.string.counter_added_successfully))
        }
        createViewModel.resetState()
    }

    private fun showLoading() {
        blockUI()
        binding.progressSave.show()
    }

    private fun showError(message: String) {
        binding.apply {
            unblockUI()
            progressSave.hide()
            root.showErrorSnackBar(message)
        }
        createViewModel.resetState()
    }

    private fun setupRecycler() {
        binding.exampleList.adapter = adapter
    }

    private fun addSections() {
        adapter.add(drinkSection())
        adapter.add(foodSection())
        adapter.add(miscSection())
    }

    private fun drinkSection(): Group {
        return ExampleSectionViewHolder(
            getString(R.string.drinks),
            drinkList.toMutableList()
        ) { titleCounter ->
            createViewModel.addCounter(titleCounter)
        }
    }

    private fun foodSection(): Group {
        return ExampleSectionViewHolder(
            getString(R.string.food),
            foodList.toMutableList()
        ) { titleCounter ->
            createViewModel.addCounter(titleCounter)
        }
    }

    private fun miscSection(): Group {
        return ExampleSectionViewHolder(
            getString(R.string.misc),
            miscList.toMutableList()
        ) { titleCounter ->
            createViewModel.addCounter(titleCounter)
        }
    }

    private fun showNetworkCreateError(){
        binding.progressSave.hide()
        unblockUI()
        createViewModel.resetState()
        createAlertDialog(
            title = getString(R.string.error_creating_counter_title,),
            message = getString(R.string.connection_error_description),
            positiveString = getString(R.string.ok),
            positiveFun = { }
        ).show()
    }

}
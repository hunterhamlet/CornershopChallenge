package com.cornershop.counterstest.views.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.navigation.fragment.findNavController
import androidx.navigation.navGraphViewModels
import com.cornershop.counterstest.R
import com.cornershop.counterstest.base.BaseFragment
import com.cornershop.counterstest.databinding.FragmentCreateCounterBinding
import com.cornershop.counterstest.extensions.addImeiAction
import com.cornershop.counterstest.extensions.addTextWatcher
import com.cornershop.counterstest.extensions.addTextWatcherTimer
import com.cornershop.counterstest.extensions.applySpannable
import com.cornershop.counterstest.extensions.applySpannableColor
import com.cornershop.counterstest.extensions.blockUI
import com.cornershop.counterstest.extensions.createAlertDialog
import com.cornershop.counterstest.extensions.disableChangedAlpha
import com.cornershop.counterstest.extensions.enableChangedAlpha
import com.cornershop.counterstest.extensions.hideKeyboard
import com.cornershop.counterstest.extensions.makeLinks
import com.cornershop.counterstest.extensions.showErrorSnackBar
import com.cornershop.counterstest.extensions.showSuccessSnackBar
import com.cornershop.counterstest.extensions.unblockUI
import com.cornershop.counterstest.sealed.UIStates
import com.cornershop.counterstest.viewmodel.CreateCounterFlowViewModel
import com.cornershop.counterstest.viewmodel.ViewModelFactory
import com.cornershop.provider.models.CounterResponse
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.koin.android.ext.android.inject

class CreateCounterFragment : BaseFragment() {

    private val binding: FragmentCreateCounterBinding by lazy {
        FragmentCreateCounterBinding.inflate(layoutInflater, null, false)
    }
    private val factoryViewModel: ViewModelFactory by inject()
    private val createViewModel: CreateCounterFlowViewModel by navGraphViewModels(R.id.nav_create) { factoryViewModel }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupOnCLickListener()
        setupObservable()
    }

    override fun onBackPressFunction() {
        super.onBackPressFunction()
        requireActivity().finish()
    }

    override fun onDetach() {
        super.onDetach()
        createViewModel.resetState()
    }

    private fun setupOnCLickListener() {
        binding.apply {
            helperLegend.makeLinks(getString(R.string.create_counter_see_example_link) to seeExampleListener)
            ivClose.setOnClickListener { requireActivity().finish() }
            buttonSave.setOnClickListener {
                saveCounter()
                hideKeyboard()
            }
            counterNameEditText.apply {
                addImeiAction { _, action, _ -> addImeAction(action) }
                addTextWatcher { counterName -> isEnableSaveButton(counterName) }
            }
        }
    }

    private fun addImeAction(actionId: Int) : Boolean {
        return when (actionId) {
            EditorInfo.IME_ACTION_DONE -> {
                hideKeyboard()
                true
            }
            else -> false
        }
    }

    private fun isEnableSaveButton(counterName: String){
        if (counterName.isNotEmpty()){
            binding.buttonSave.enableChangedAlpha()
        } else {
            binding.buttonSave.disableChangedAlpha()
        }
    }

    private fun setupObservable() {
        createViewModel.uiObservableState.observer { state -> handleState(state) }
    }

    private fun handleState(state: UIStates<MutableList<CounterResponse>>) {
        when (state) {
            is UIStates.Loading -> showLoading()
            is UIStates.HasContent -> hasContent()
            is UIStates.Error -> showError(state.message)
            is UIStates.ShowNetworkCreateError -> showNetworkCreateError()
        }
    }

    private fun hasContent() {
        binding.apply {
            unblockUI()
            progressSave.hide()
            root.showSuccessSnackBar(getString(R.string.counter_added_successfully))
            createViewModel.resetState()
        }
    }

    private fun showError(message: String) {
        binding.apply {
            unblockUI()
            progressSave.hide()
            root.showErrorSnackBar(message)
            createViewModel.resetState()
        }
    }

    private fun showLoading() {
        blockUI()
        binding.progressSave.show()
    }

    private fun saveCounter() {
        if (checkEmptyCounter().not()) {
            createViewModel.addCounter(binding.counterNameEditText.text.toString())
        } else {
            binding.showErrorSnackBar(getString(R.string.create_counter_not_empty))
        }
    }

    private fun checkEmptyCounter(): Boolean =
        binding.counterNameEditText.text.toString().isEmpty()

    private val seeExampleListener =
        View.OnClickListener { findNavController().navigate(R.id.action_createFragment_to_exampleFragment) }

    private fun showNetworkCreateError(){
        binding.progressSave.hide()
        unblockUI()
        createViewModel.resetState()
        createAlertDialog(
            title = getString(R.string.error_creating_counter_title,),
            message = getString(R.string.connection_error_description),
            positiveString = getString(R.string.ok),
            positiveFun = { }
        ).show()
    }
}
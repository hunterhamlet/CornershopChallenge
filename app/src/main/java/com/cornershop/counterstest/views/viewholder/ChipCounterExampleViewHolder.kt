package com.cornershop.counterstest.views.viewholder

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.cornershop.counterstest.R
import com.cornershop.counterstest.databinding.ItemChipExampleCounterBinding
import com.cornershop.counterstest.extensions.getDimension
import com.cornershop.counterstest.util.FunReturnString
import com.xwray.groupie.viewbinding.BindableItem

class ChipCounterExampleViewHolder(
    private val title: String,
    private val chipTap: FunReturnString
) : BindableItem<ItemChipExampleCounterBinding>() {

    override fun bind(viewBinding: ItemChipExampleCounterBinding, position: Int) {
        viewBinding.apply {
            val layoutParams = ConstraintLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            Log.d("JHMM", "position: $position")
            if (position == 0) {
                layoutParams.marginStart = getDimension(R.dimen.ds_space_2_5).toInt()
                layoutParams.marginEnd = getDimension(R.dimen.ds_space_1).toInt()
            } else {
                layoutParams.marginStart = getDimension(R.dimen.ds_space_1).toInt()
                layoutParams.marginEnd = getDimension(R.dimen.ds_space_1).toInt()
            }
            containerChip.layoutParams = layoutParams
            chip.text = title
            containerChip.setOnClickListener { chipTap.invoke(title) }
        }
    }

    override fun getLayout(): Int = R.layout.item_chip_example_counter

    override fun initializeViewBinding(view: View): ItemChipExampleCounterBinding =
        ItemChipExampleCounterBinding.bind(view)
}
package com.cornershop.counterstest.views.activity

import android.content.Intent
import android.os.Bundle
import com.cornershop.counterstest.base.BaseActivity
import com.cornershop.counterstest.databinding.ActivityWelcomeBinding
import com.cornershop.provider.repository.local.Shared

class WelcomeActivity : BaseActivity() {

    private val binding: ActivityWelcomeBinding by lazy {
        ActivityWelcomeBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupOnCLickListener()
    }

    private fun setupOnCLickListener() {
        binding.layoutWelcomeContent.buttonStart.setOnClickListener {
            Shared.isFirstTime(this, false)
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

}

package com.cornershop.counterstest.views.activity

import android.content.Intent
import android.os.Bundle
import com.cornershop.counterstest.base.BaseActivity
import com.cornershop.provider.repository.local.Shared

class SplashScreen : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkIfIsFirstTime()
    }

    private fun checkIfIsFirstTime() {
        if (Shared.isFirstTime(this)) {
            startActivity(Intent(this, WelcomeActivity::class.java))
        } else {
            startActivity(Intent(this, MainActivity::class.java))
        }
        finish()
    }

}
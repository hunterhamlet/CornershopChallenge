package com.cornershop.counterstest.views.viewholder

import android.view.View
import com.cornershop.counterstest.R
import com.cornershop.counterstest.databinding.ItemExampleSectionViewHolderBinding
import com.cornershop.counterstest.extensions.setup
import com.cornershop.counterstest.util.FunReturnString
import com.xwray.groupie.GroupieAdapter
import com.xwray.groupie.viewbinding.BindableItem

class ExampleSectionViewHolder(
    private val titleSection: String,
    private val listString: MutableList<String>,
    private val chipTap: FunReturnString
) : BindableItem<ItemExampleSectionViewHolderBinding>() {

    private val adapter: GroupieAdapter by lazy { GroupieAdapter() }

    override fun bind(viewBinding: ItemExampleSectionViewHolderBinding, position: Int) {
        viewBinding.apply {
            chipList.adapter = adapter
            sectionName.text = titleSection
            adapter.apply {
                clear()
                addAll(listString.map {
                    ChipCounterExampleViewHolder(it) { tapChip ->
                        chipTap.invoke(tapChip)
                    }
                })
            }
        }
    }

    override fun getLayout(): Int = R.layout.item_example_section_view_holder

    override fun initializeViewBinding(view: View): ItemExampleSectionViewHolderBinding =
        ItemExampleSectionViewHolderBinding.bind(view)
}
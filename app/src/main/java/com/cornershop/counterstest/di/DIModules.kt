package com.cornershop.counterstest.di

import android.content.Context
import com.cornershop.counterstest.viewmodel.CreateCounterFlowViewModel
import com.cornershop.counterstest.viewmodel.GenericMainViewModel
import com.cornershop.counterstest.viewmodel.MainViewModel
import com.cornershop.counterstest.viewmodel.SearchViewModel
import com.cornershop.counterstest.viewmodel.ViewModelFactory
import com.cornershop.provider.di.ProviderDI
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

object DIModules {

    fun init(application: Context) {
        startKoin {
            androidLogger()
            androidContext(application)
            modules(
                listOf(
                    ProviderDI.servicesModule,
                    ProviderDI.usesCases,
                    ProviderDI.repositories,
                    viewModelModule
                )
            )
        }
    }

    private val viewModelModule = module {
        factory { ViewModelFactory(get(), get(), get(), get(), get(), get()) }
        viewModel { GenericMainViewModel() }
        viewModel { MainViewModel(get(), get(), get(), get()) }
        viewModel { CreateCounterFlowViewModel(get()) }
        viewModel { SearchViewModel() }
    }

}
package com.cornershop.counterstest.viewmodel

import androidx.databinding.ObservableInt
import com.cornershop.counterstest.base.BaseViewModel
import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.usecases.DecrementCounterUseCase
import com.cornershop.provider.usecases.IncrementCounterUseCase

class ProductViewModel(
    private val counterItem: CounterResponse,
    private val incrementCounterUseCase: IncrementCounterUseCase,
    private val decrementCounterUseCase: DecrementCounterUseCase
) : BaseViewModel() {

    private var counterItemCount: Int = counterItem.count

    val counter: ObservableInt = ObservableInt(counterItemCount)

    fun incrementCount() {
        counter.set(counterItemCount++)
    }

    fun decrementCount() {
        counter.set(counterItemCount--)
    }

}
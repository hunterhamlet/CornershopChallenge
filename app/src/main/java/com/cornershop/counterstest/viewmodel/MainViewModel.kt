package com.cornershop.counterstest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cornershop.counterstest.base.BaseViewModel
import com.cornershop.counterstest.extensions.isDataConnected
import com.cornershop.counterstest.sealed.UIStates
import com.cornershop.counterstest.util.TypeOperation
import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.usecases.DecrementCounterUseCase
import com.cornershop.provider.usecases.DeleteCounterUseCase
import com.cornershop.provider.usecases.GetCountersUseCase
import com.cornershop.provider.usecases.IncrementCounterUseCase
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class MainViewModel(
    private val getCounterUseCase: GetCountersUseCase,
    private val incrementCounterUseCase: IncrementCounterUseCase,
    private val decrementCounterUseCase: DecrementCounterUseCase,
    private val deleteCounterUseCase: DeleteCounterUseCase
) : BaseViewModel() {

    private val _uiObservableState: MutableLiveData<UIStates<MutableList<CounterResponse>>> by lazy {
        MutableLiveData<UIStates<MutableList<CounterResponse>>>().apply { value = UIStates.Init }
    }
    val uiObservableState: LiveData<UIStates<MutableList<CounterResponse>>> get() = _uiObservableState
    private val cacheCounterList: MutableList<CounterResponse> = mutableListOf()
    private val selectedCounterList: MutableList<CounterResponse> = mutableListOf()

    fun getCounters() {
        viewModelScope.launch {
            handleApiResult(getCounterUseCase())
        }
    }

    private fun handleGetCountersSuccess(listCounter: MutableList<CounterResponse>) {
        if (listCounter.isNotEmpty()) {
            cacheCounterList.clear()
            cacheCounterList.addAll(listCounter)
            _uiObservableState.value =
                UIStates.HasContent(data = cacheCounterList)
        } else {
            _uiObservableState.value = UIStates.NoContent
        }
    }

    private fun handleError(message: String) {
        _uiObservableState.value = UIStates.Error(message)
    }

    fun incrementCounter(counter: CounterResponse) {
        viewModelScope.launch {
            if (isDataConnected()) {
                handleApiResult(incrementCounterUseCase(counterId = counter.id))
            } else {
                _uiObservableState.value =
                    UIStates.ShowErrorNetworkCounterOperation(counter, TypeOperation.INCREMENT)
            }
        }
    }

    fun decrementCounter(counter: CounterResponse) {
        viewModelScope.launch {
            if (isDataConnected()) {
                handleApiResult(decrementCounterUseCase(counterId = counter.id))
            } else {
                _uiObservableState.value =
                    UIStates.ShowErrorNetworkCounterOperation(counter, TypeOperation.DECREMENT)
            }
        }
    }

    private fun handleApiResult(apiResult: APIResult<MutableList<CounterResponse>>) {
        when (apiResult) {
            is APIResult.Success -> handleGetCountersSuccess(apiResult.data)
            is APIResult.Failure -> handleError(apiResult.error)
        }
    }

    fun searchCounters(query: String) {
        if (query.isNotEmpty()) {
            val listFiltered =
                cacheCounterList.filter { counter ->
                    counter.title.contains(
                        query,
                        ignoreCase = true
                    )
                }
                    .toMutableList()
            searchStateApply(listFiltered)
        } else {
            _uiObservableState.postValue(UIStates.HasContent(data = cacheCounterList))
        }
    }

    private fun searchStateApply(listFiltered: MutableList<CounterResponse>) {
        if (listFiltered.isEmpty().not()) {
            _uiObservableState.postValue(UIStates.HasContent(data = listFiltered))
        } else {
            _uiObservableState.postValue(UIStates.NoContentSearch)
        }
    }

    fun addCounterInDeleteList(counter: CounterResponse) {
        selectedCounterList.add(counter)
        _uiObservableState.value = UIStates.DeleteCounters(selectedCounterList)
    }

    fun removeCounterInDeleteList(counter: CounterResponse) {
        selectedCounterList.remove(counter)
        _uiObservableState.value = UIStates.DeleteCounters(selectedCounterList)
    }

    fun resetSelectedList() {
        selectedCounterList.clear()
    }

    fun deleteItems() {
        _uiObservableState.value = UIStates.Loading
        viewModelScope.launch() {
            if (isDataConnected()) {
                selectedCounterList.forEach { counter ->
                    val response =
                        async(Dispatchers.IO) { deleteCounterUseCase(counterId = counter.id) }
                    response.await()
                }
                resetSelectedList()
                getCounters()
            } else {
                _uiObservableState.value = UIStates.ShowNetworkDeleteError
            }
        }
    }

    fun deleteItemWarning() {
        if (selectedCounterList.size == 1) {
            _uiObservableState.value = UIStates.ShowDeleteWarning(selectedCounterList.first())
        } else {
            _uiObservableState.value = UIStates.ShowDeleteListWarning
        }
    }

    fun shareCounterList() {
        _uiObservableState.value = UIStates.ShareContent(Gson().toJson(selectedCounterList))
    }

    fun selectCounterOperation(counter: CounterResponse, typeOperation: TypeOperation) {
        when (typeOperation) {
            TypeOperation.INCREMENT -> incrementCounter(counter)
            TypeOperation.DECREMENT -> decrementCounter(counter)
        }
    }

    fun resetState() {
        _uiObservableState.value = UIStates.Init
    }

}
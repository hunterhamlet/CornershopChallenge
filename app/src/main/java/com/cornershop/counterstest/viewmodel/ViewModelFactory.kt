package com.cornershop.counterstest.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cornershop.provider.usecases.AddCounterUseCase
import com.cornershop.provider.usecases.DecrementCounterUseCase
import com.cornershop.provider.usecases.DeleteCounterUseCase
import com.cornershop.provider.usecases.GetCountersUseCase
import com.cornershop.provider.usecases.IncrementCounterUseCase

class ViewModelFactory(
    private val app: Application,
    private val getCountersUseCase: GetCountersUseCase,
    private val incrementCounterUseCase: IncrementCounterUseCase,
    private val decrementCounterUseCase: DecrementCounterUseCase,
    private val addCounterUseCase: AddCounterUseCase,
    private val deleteCounterUseCase: DeleteCounterUseCase
) :
    ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return with(modelClass) {
            when {
                isAssignableFrom(GenericMainViewModel::class.java) -> GenericMainViewModel()
                isAssignableFrom(MainViewModel::class.java) -> MainViewModel(
                    getCountersUseCase,
                    incrementCounterUseCase,
                    decrementCounterUseCase,
                    deleteCounterUseCase
                )
                isAssignableFrom(CreateCounterFlowViewModel::class.java) -> CreateCounterFlowViewModel(addCounterUseCase)
                else -> throw IllegalArgumentException("Unknown ViewModel class you must add it") as Throwable
            }
        } as T
    }
}
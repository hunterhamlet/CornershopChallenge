package com.cornershop.counterstest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.cornershop.counterstest.base.BaseViewModel
import com.cornershop.counterstest.extensions.StringFun

class GenericMainViewModel: BaseViewModel() {

    private val _loadingState : MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>().apply { value = false }
    }
    val loadingState : LiveData<Boolean> get() = _loadingState

    private val _showError : MutableLiveData<String> by lazy {
        MutableLiveData<String>().apply { value = StringFun.emptyString() }
    }
    val showError : LiveData<String> get() = _showError

    private val _showSuccess : MutableLiveData<String> by lazy {
        MutableLiveData<String>().apply { value = StringFun.emptyString() }
    }
    val showSuccess : LiveData<String> get() = _showSuccess

    private val _onBackPress : MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>().apply { value = false }
    }
    val onBackPress : LiveData<Boolean> get() = _onBackPress

    fun showLoading(){
        _loadingState.value = true
    }

    fun hideLoading(){
        _loadingState.value = false
    }

    fun onBackPressed(){
        _onBackPress.value = true
    }


}
package com.cornershop.counterstest.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.cornershop.counterstest.base.BaseViewModel
import com.cornershop.counterstest.extensions.isDataConnected
import com.cornershop.counterstest.sealed.UIStates
import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.usecases.AddCounterUseCase
import kotlinx.coroutines.launch

class CreateCounterFlowViewModel(
    private val addCounterUseCase: AddCounterUseCase,
) : BaseViewModel() {

    private val _uiObservableState: MutableLiveData<UIStates<MutableList<CounterResponse>>> by lazy {
        MutableLiveData<UIStates<MutableList<CounterResponse>>>().apply { value = UIStates.Init }
    }
    val uiObservableState: LiveData<UIStates<MutableList<CounterResponse>>> get() = _uiObservableState

    fun addCounter(title: String){
        _uiObservableState.value = UIStates.Loading
        checkConnectionAndCreateCounter(title)
    }

    private fun checkConnectionAndCreateCounter(title: String){
        if (isDataConnected()) {
            createCounter(title)
        } else {
            _uiObservableState.value = UIStates.ShowNetworkCreateError
        }
    }

    private fun createCounter(title: String){
        viewModelScope.launch {
            val result = addCounterUseCase(title)
            handleAddedState(result)
        }
    }

    private fun handleAddedState(state: APIResult<MutableList<CounterResponse>>){
        when (state){
            is APIResult.Success -> _uiObservableState.value = UIStates.HasContent(data = state.data)
            is APIResult.Failure -> _uiObservableState.value = UIStates.Error(message = state.error)
        }
    }

    fun resetState(){
        _uiObservableState.value = UIStates.Init
    }

}
package com.cornershop.counterstest.extensions

import android.app.Activity
import android.content.Context
import android.graphics.Typeface
import android.text.Selection
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.cornershop.counterstest.R
import com.cornershop.counterstest.util.FunOnlyInvoke
import com.cornershop.counterstest.util.FunReturnString
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import java.util.Timer
import kotlin.concurrent.schedule

fun AppCompatActivity.blockUI() {
    window.setFlags(
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
    )
}

fun AppCompatActivity.unblockUI() {
    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
}

fun View.showSuccessSnackBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG)
        .setBackgroundTint(ContextCompat.getColor(this.context, android.R.color.holo_green_dark))
        .setTextColor(ContextCompat.getColor(this.context, android.R.color.white))
        .show()
}

fun View.showErrorSnackBar(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG)
        .setBackgroundTint(ContextCompat.getColor(this.context, android.R.color.holo_red_light))
        .setTextColor(ContextCompat.getColor(this.context, android.R.color.white))
        .show()
}

fun ViewBinding.showSuccessSnackBar(message: String) {
    Snackbar.make(this.root, message, Snackbar.LENGTH_LONG)
        .setBackgroundTint(
            ContextCompat.getColor(
                this.root.context,
                android.R.color.holo_green_dark
            )
        )
        .setTextColor(ContextCompat.getColor(this.root.context, android.R.color.white))
        .show()
}

fun ViewBinding.showErrorSnackBar(message: String) {
    Snackbar.make(this.root, message, Snackbar.LENGTH_LONG)
        .setBackgroundTint(
            ContextCompat.getColor(
                this.root.context,
                android.R.color.holo_red_light
            )
        )
        .setTextColor(ContextCompat.getColor(this.root.context, android.R.color.white))
        .show()
}

fun Fragment.blockUI() {
    requireActivity().window.setFlags(
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
    )
}

fun Fragment.unblockUI() {
    requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
}

fun Activity.blockUI() {
    this.window.setFlags(
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
    )
}

fun Activity.unblockUI() {
    this.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
}

fun Activity.blockUIState(isBlock: Boolean) {
    if (isBlock) {
        blockUI()
    } else {
        unblockUI()
    }
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Context.showKeyboard() {
    val inputMethodManager: InputMethodManager =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
}

fun ViewBinding.getString(@StringRes idString: Int): String {
    return this.root.context.getString(idString)
}

fun ViewBinding.getColor(@ColorRes idColor: Int): Int {
    return ContextCompat.getColor(this.root.context, idColor)
}

fun ViewBinding.toast(message: String) {
    Toast.makeText(this.root.context, message, Toast.LENGTH_SHORT).show()
}

fun ViewBinding.toastLong(message: String) {
    Toast.makeText(this.root.context, message, Toast.LENGTH_LONG).show()
}

fun ViewBinding.getDimension(@DimenRes id: Int) = root.resources.getDimension(id)

fun Context.getColor(@ColorRes idColor: Int): Int {
    return ContextCompat.getColor(this, idColor)
}

fun AppCompatTextView.makeLinks(vararg links: Pair<String, View.OnClickListener>) {
    val spannableString = SpannableString(this.text)
    for (link in links) {
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(view: View) {
                Selection.setSelection((view as TextView).text as Spannable, 0)
                view.invalidate()
                link.second.onClick(view)
            }
        }
        val startIndexOfLink = this.text.toString().indexOf(link.first)
        spannableString.setSpan(
            clickableSpan, startIndexOfLink, startIndexOfLink + link.first.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
    this.movementMethod =
        LinkMovementMethod.getInstance() // without LinkMovementMethod, link can not click
    this.setText(spannableString, TextView.BufferType.SPANNABLE)
}

fun <T : RecyclerView.ViewHolder?> RecyclerView.setup(adapter: RecyclerView.Adapter<T>) {
    this.apply {
        this.adapter = adapter
        this.layoutManager = LinearLayoutManager(context)
    }
}

fun <T : RecyclerView.ViewHolder?> RecyclerView.setup(
    adapter: RecyclerView.Adapter<T>,
    isHorizontal: Boolean = false
) {
    this.apply {
        this.adapter = adapter
        this.layoutManager = LinearLayoutManager(context)
    }
}

fun TextInputEditText.addImeiAction(function: (view: View, actionId: Int, keyEvent: KeyEvent?) -> Boolean) {
    this.setOnEditorActionListener { view, actionId, keyEvent ->
        return@setOnEditorActionListener function.invoke(view, actionId, keyEvent)
    }

}

fun TextInputEditText.addTextWatcherTimer(delayTimeInMs: Long = 500, function: FunReturnString) {
    var timer = Timer("doSomething", false)
    this.doOnTextChanged { text, _, _, _ ->
        timer.cancel()
        timer = Timer("doSomething", false)
        timer.schedule(delayTimeInMs) {
            function.invoke(text.toString())
        }
    }
}

fun TextInputEditText.addTextWatcher(function: FunReturnString) {
    this.doOnTextChanged { text, _, _, _ -> function.invoke(text.toString()) }
}

fun View.visibility(visibility: Boolean) {
    if (visibility) this.show() else this.hide()
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.hide() {
    this.visibility = View.GONE
}

fun View.enable() {
    this.isEnabled = true
}

fun View.disable() {
    this.isEnabled = false
}

fun View.enableChangedAlpha() {
    this.apply {
        isEnabled = true
        alpha = 1f
    }
}

fun View.disableChangedAlpha() {
    this.apply {
        isEnabled = false
        alpha = 0.5f
    }
}

fun String.applySpannable(startIndex: Int, endIndex: Int): Spannable {
    val spannableString = SpannableString(this)
    spannableString.setSpan(
        StyleSpan(Typeface.BOLD),
        startIndex,
        endIndex,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )
    return spannableString
}

fun String.applySpannable(): Spannable {
    val spannableString = SpannableString(this)
    spannableString.setSpan(
        StyleSpan(Typeface.BOLD),
        0,
        this.length,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )
    return spannableString
}

fun String.applySpannableColor(context: Context, @ColorRes color: Int): Spannable {
    val spannableString = SpannableString(this)
    spannableString.setSpan(
        ForegroundColorSpan(ContextCompat.getColor(context, color)),
        0,
        this.length,
        Spannable.SPAN_INCLUSIVE_EXCLUSIVE
    )
    return spannableString
}

fun Fragment.createAlertDialog(
    @StringRes title: Int? = null,
    @StringRes message: Int? = null,
    @StringRes positiveString: Int? = null,
    @StringRes negativeString: Int? = null,
    positiveFun: FunOnlyInvoke? = null,
    negativeFun: FunOnlyInvoke? = null
): MaterialAlertDialogBuilder {
    val dialogBuilder = MaterialAlertDialogBuilder(requireContext(), R.style.CustomAlertDialog)
    title?.let { dialogBuilder.setTitle(getString(it).applySpannable()) }
    message?.let {
        dialogBuilder.setMessage(
            getString(it).applySpannableColor(
                requireContext(),
                R.color.gray
            )
        )
    }
    positiveString?.let {
        dialogBuilder.setPositiveButton(getString(positiveString)) { _, _ ->
            positiveFun?.invoke()
        }
    }
    negativeString?.let {
        dialogBuilder.setNegativeButton(getString(negativeString)) { _, _ ->
            negativeFun?.invoke()
        }
    }
    return dialogBuilder
}

fun Fragment.createAlertDialog(
    title: String? = null,
    message: String? = null,
    positiveString: String? = null,
    negativeString: String? = null,
    positiveFun: FunOnlyInvoke? = null,
    negativeFun: FunOnlyInvoke? = null
): MaterialAlertDialogBuilder {
    val dialogBuilder = MaterialAlertDialogBuilder(requireContext(), R.style.CustomAlertDialog)
    title?.let { dialogBuilder.setTitle(title.applySpannable()) }
    message?.let {
        dialogBuilder.setMessage(
            message.applySpannableColor(
                requireContext(),
                R.color.gray
            )
        )
    }
    positiveString?.let {
        dialogBuilder.setPositiveButton(positiveString) { _, _ ->
            positiveFun?.invoke()
        }
    }
    negativeString?.let {
        dialogBuilder.setNegativeButton(negativeString) { _, _ ->
            negativeFun?.invoke()
        }
    }
    return dialogBuilder
}
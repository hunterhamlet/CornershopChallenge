package com.cornershop.counterstest.extensions

import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import java.lang.Exception


object StringFun {

    fun emptyString(): String = ""

}

fun isDataConnected(): Boolean {
    try {
        val p1 = Runtime.getRuntime().exec("ping -c 1 8.8.8.8")
        val returnVal = p1.waitFor()
        return returnVal == 0
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return false
}
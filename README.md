## Counter test
This is will help you to count your ideas, tou can add a new idea, increase or substrac the counter of you ideas. In the same way we can remove and share yout ideas.

## Architecture
This app is build with MVVM, clean architecture and MVI as auxiliary, contains three principal modules:
* app: Contains all views (Activity, Fragment, ViewHolder), base, DI, extensions, sealed, util, viewmodel.
* provider: Contains repositories, usescases, models, services, api result, di and api build.
* style: Contains all drawables, colors and styles.

## Test
For test this app, is need start local server, include in this repo, only need follow the next step, (You should have npm installed):
* Go to folder "server"  from the terminal and writte: `npm install && npm start`.
* If you will run application in an emulator, only need compile and run in emulator.
* If you will run application in a physical device, you should know IP address from pc where the local server si running, and change this IP, in provider gradle file (BASE_URL)

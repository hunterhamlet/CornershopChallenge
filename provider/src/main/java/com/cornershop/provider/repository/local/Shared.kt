package com.cornershop.provider.repository.local

import android.content.Context

object Shared {

    private const val PREFERENCES = "app_preferences"
    private const val IS_FIRST_TIME = "is_first_time"

    fun isFirstTime(context: Context, isFirstTime: Boolean) {
        context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)
            .edit()
            .apply {
                putBoolean(IS_FIRST_TIME, isFirstTime)
            }.apply()
    }

    fun isFirstTime(context: Context): Boolean {
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)
            .getBoolean(IS_FIRST_TIME, true)
    }

}
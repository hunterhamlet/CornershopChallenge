package com.cornershop.provider.repository.remote

import com.cornershop.provider.models.CounterRequestId
import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.services.CounterServices

internal class IncrementCounterRepositoryImpl (private val api: CounterServices) : IncrementCounterRepository {
    override suspend operator fun invoke(counterId: String): APIResult<MutableList<CounterResponse>> {
        return try {
            val response = api.incrementCounter(CounterRequestId(counterId))
            if (response.isSuccessful) {
                response.body()?.let {
                    APIResult.Success(data = it)
                } ?: run {
                    APIResult.Success(data = mutableListOf())
                }
            } else {
                APIResult.Failure(error = "Problems with server, try late.")
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
            APIResult.Failure(error = "Problems with server, try late.")
        }
    }
}

interface IncrementCounterRepository {
    suspend operator fun invoke(counterId: String): APIResult<MutableList<CounterResponse>>
}
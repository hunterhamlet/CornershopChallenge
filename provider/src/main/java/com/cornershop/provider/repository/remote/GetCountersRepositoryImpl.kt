package com.cornershop.provider.repository.remote

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.services.CounterServices

internal class GetCountersRepositoryImpl(private val api: CounterServices) : GetCountersRepository {
    override suspend operator fun invoke(): APIResult<MutableList<CounterResponse>> {
        return try {
            val response = api.getCounters()
            if (response.isSuccessful) {
                response.body()?.let {
                    APIResult.Success(data = it)
                } ?: run {
                    APIResult.Success(data = mutableListOf())
                }
            } else {
                APIResult.Failure(error = "Problems with server, try late.")
            }
        } catch (exception: Exception) {
            exception.printStackTrace()
            APIResult.Failure(error = "Problems with server, try late.")
        }
    }
}

interface GetCountersRepository {
    suspend operator fun invoke(): APIResult<MutableList<CounterResponse>>
}
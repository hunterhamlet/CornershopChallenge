package com.cornershop.provider.models

import com.squareup.moshi.Json

data class CounterResponse(
    @field:Json(name = "id") val id: String = "",
    @field:Json(name = "title") val title: String = "",
    @field:Json(name = "count") val count: Int = 0
)
package com.cornershop.provider.usecases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.IncrementCounterRepository
import com.cornershop.provider.sealed.APIResult

class IncrementCounterUseCase(private val incrementCounterRepository: IncrementCounterRepository) {
    suspend operator fun invoke(counterId: String): APIResult<MutableList<CounterResponse>> =
        incrementCounterRepository(counterId)

}
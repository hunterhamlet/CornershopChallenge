package com.cornershop.provider.usecases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.DecrementCounterRepository
import com.cornershop.provider.sealed.APIResult

class DecrementCounterUseCase(private val decrementCounterRepository: DecrementCounterRepository) {
    suspend operator fun invoke(counterId: String): APIResult<MutableList<CounterResponse>> =
        decrementCounterRepository(counterId)
}
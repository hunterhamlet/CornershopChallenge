package com.cornershop.provider.usecases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.DeleteCounterRepository
import com.cornershop.provider.sealed.APIResult

class DeleteCounterUseCase(private val deleteCounterRepository: DeleteCounterRepository) {
    suspend operator fun invoke(counterId: String): APIResult<MutableList<CounterResponse>> =
        deleteCounterRepository(counterId)
}
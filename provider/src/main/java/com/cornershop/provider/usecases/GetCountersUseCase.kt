package com.cornershop.provider.usecases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.GetCountersRepository
import com.cornershop.provider.sealed.APIResult

class GetCountersUseCase(private val getCountersRepository: GetCountersRepository) {

    suspend operator fun invoke(): APIResult<MutableList<CounterResponse>> = getCountersRepository()

}
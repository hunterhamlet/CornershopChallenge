package com.cornershop.provider.usecases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.AddCounterRepository
import com.cornershop.provider.sealed.APIResult

class AddCounterUseCase(private val addCounterRepository: AddCounterRepository) {
    suspend operator fun invoke(title: String): APIResult<MutableList<CounterResponse>> =
        addCounterRepository(title)
}
package com.cornershop.provider.sealed

enum class TypeNoContent {
    COMMON_TYPE, SEARCH_TYPE
}
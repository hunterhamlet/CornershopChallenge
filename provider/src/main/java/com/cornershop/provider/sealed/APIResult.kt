package com.cornershop.provider.sealed

sealed class APIResult<out T : Any> {
    data class Success<T : Any>(val data: T) : APIResult<T>()
    data class Failure(val error: String) : APIResult<Nothing>()
}

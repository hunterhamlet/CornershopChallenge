package com.cornershop.provider.di

import com.cornershop.provider.API
import com.cornershop.provider.repository.remote.AddCounterRepository
import com.cornershop.provider.repository.remote.AddCounterRepositoryImpl
import com.cornershop.provider.repository.remote.DecrementCounterRepository
import com.cornershop.provider.repository.remote.DecrementCounterRepositoryImpl
import com.cornershop.provider.repository.remote.DeleteCounterRepository
import com.cornershop.provider.repository.remote.DeleteCounterRepositoryImpl
import com.cornershop.provider.repository.remote.GetCountersRepository
import com.cornershop.provider.repository.remote.GetCountersRepositoryImpl
import com.cornershop.provider.repository.remote.IncrementCounterRepository
import com.cornershop.provider.repository.remote.IncrementCounterRepositoryImpl
import com.cornershop.provider.services.CounterServices
import com.cornershop.provider.usecases.AddCounterUseCase
import com.cornershop.provider.usecases.DecrementCounterUseCase
import com.cornershop.provider.usecases.DeleteCounterUseCase
import com.cornershop.provider.usecases.GetCountersUseCase
import com.cornershop.provider.usecases.IncrementCounterUseCase
import org.koin.dsl.module

object ProviderDI {

    val servicesModule = module {
        factory { API.getServices<CounterServices>() }
    }

    val usesCases = module {
        factory { GetCountersUseCase(get()) }
        factory { AddCounterUseCase(get()) }
        factory { DeleteCounterUseCase(get()) }
        factory { IncrementCounterUseCase(get()) }
        factory { DecrementCounterUseCase(get()) }
    }

    val repositories = module {
        factory<AddCounterRepository> { AddCounterRepositoryImpl(get()) }
        factory<GetCountersRepository> { GetCountersRepositoryImpl(get()) }
        factory<DecrementCounterRepository> { DecrementCounterRepositoryImpl(get()) }
        factory<IncrementCounterRepository> { IncrementCounterRepositoryImpl(get()) }
        factory<DeleteCounterRepository> { DeleteCounterRepositoryImpl(get()) }
    }
}
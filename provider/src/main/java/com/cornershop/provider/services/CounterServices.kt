package com.cornershop.provider.services

import com.cornershop.provider.models.CounterRequestId
import com.cornershop.provider.models.CounterRequestTitle
import com.cornershop.provider.models.CounterResponse
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.Headers
import retrofit2.http.POST

interface CounterServices {

    @Headers("Content-Type: application/json")
    @GET(GET_COUNTERS)
    suspend fun getCounters(): Response<MutableList<CounterResponse>>

    @Headers("Content-Type: application/json")
    @POST(OPERATIONS_IN_COUNTER)
    suspend fun addCounter(@Body requestTitle: CounterRequestTitle): Response<MutableList<CounterResponse>>

    @Headers("Content-Type: application/json")
    @POST(INCREMENT_COUNTER)
    suspend fun incrementCounter(@Body requestId: CounterRequestId): Response<MutableList<CounterResponse>>

    @Headers("Content-Type: application/json")
    @POST(DECREMENT_COUNTER)
    suspend fun decrementCounter(@Body requestId: CounterRequestId): Response<MutableList<CounterResponse>>

    @Headers("Content-Type: application/json")
    @HTTP(method = "DELETE", path = OPERATIONS_IN_COUNTER, hasBody = true)
    suspend fun deleteCounter(@Body requestId: CounterRequestId): Response<MutableList<CounterResponse>>

}
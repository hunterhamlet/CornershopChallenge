package com.cornershop.provider.services

const val GET_COUNTERS = "/api/v1/counters/"
const val OPERATIONS_IN_COUNTER = "/api/v1/counter/"
const val INCREMENT_COUNTER = "/api/v1/counter/inc/"
const val DECREMENT_COUNTER = "/api/v1/counter/dec/"
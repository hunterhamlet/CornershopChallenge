package com.cornershop.provider

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object API {

    inline fun <reified T> getServices(host: String = BuildConfig.BASE_URL): T {

        val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        val client: OkHttpClient = OkHttpClient
            .Builder()
            .addInterceptor(interceptor)
            .build()

        return Retrofit
            .Builder()
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .baseUrl(host).build().create(T::class.java)
    }

}
package com.cornershop.provider.usesCases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.AddCounterRepository
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.usecases.AddCounterUseCase
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class AddCounterUseCaseTest {

    @Mock
    lateinit var addCounterRepository: AddCounterRepository

    lateinit var addCounterUseCase: AddCounterUseCase

    private val counterTitle: String = "Cup of coffe"
    private val listResponse: MutableList<CounterResponse> =
        mutableListOf(CounterResponse(id = "qwerty", title = counterTitle, count = 0))
    private val successResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Success(data = listResponse)
    private val errorResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Failure(error = "Problems with server, try late.")

    @Before
    fun setup() {
        addCounterUseCase = AddCounterUseCase(addCounterRepository)
    }

    @Test
    fun `should add counter success`() {
        runBlockingTest {
            whenever(addCounterRepository.invoke(counterTitle)).thenReturn(successResponse)
            val result = addCounterUseCase(counterTitle)
            assertEquals(result,successResponse)
        }
    }

    @Test
    fun `should add counter error`() {
        runBlockingTest {
            whenever(addCounterRepository.invoke(counterTitle)).thenReturn(errorResponse)
            val result = addCounterUseCase(counterTitle)
            assertEquals(result, errorResponse)
        }
    }

}
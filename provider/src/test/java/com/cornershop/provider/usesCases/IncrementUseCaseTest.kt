package com.cornershop.provider.usesCases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.AddCounterRepository
import com.cornershop.provider.repository.remote.IncrementCounterRepository
import com.cornershop.provider.repository.remote.IncrementCounterRepositoryImpl
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.usecases.AddCounterUseCase
import com.cornershop.provider.usecases.IncrementCounterUseCase
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class IncrementUseCaseTest {
    @Mock
    lateinit var incrementCounterRepository: IncrementCounterRepository

    lateinit var incrementCounterUseCase: IncrementCounterUseCase

    private val counterID: String = "qwerty"
    private val listResponse: MutableList<CounterResponse> =
        mutableListOf(CounterResponse(id = "qwerty", title = "Cup of coffee", count = 1))
    private val successResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Success(data = listResponse)
    private val errorResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Failure(error = "Problems with server, try late.")

    @Before
    fun setup() {
        incrementCounterUseCase = IncrementCounterUseCase(incrementCounterRepository)
    }

    @Test
    fun `should increment counter success`() {
        runBlockingTest {
            whenever(incrementCounterRepository.invoke(counterID)).thenReturn(successResponse)
            val result = incrementCounterUseCase(counterID)
            assertEquals(result,successResponse)
        }
    }

    @Test
    fun `should increment counter error`() {
        runBlockingTest {
            whenever(incrementCounterRepository.invoke(counterID)).thenReturn(errorResponse)
            val result = incrementCounterUseCase(counterID)
            assertEquals(result, errorResponse)
        }
    }
}
package com.cornershop.provider.usesCases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.AddCounterRepository
import com.cornershop.provider.repository.remote.DeleteCounterRepository
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.usecases.AddCounterUseCase
import com.cornershop.provider.usecases.DeleteCounterUseCase
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DeleteUseCaseTest {

    @Mock
    lateinit var deleteCounterRepository: DeleteCounterRepository

    lateinit var deleteCounterUseCase: DeleteCounterUseCase

    private val counterID: String = "qwerty"
    private val listResponse: MutableList<CounterResponse> =
        mutableListOf(CounterResponse(id = "qwerty", title = "Cup of coffe", count = 0))
    private val successResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Success(data = listResponse)
    private val errorResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Failure(error = "Problems with server, try late.")

    @Before
    fun setup() {
        deleteCounterUseCase = DeleteCounterUseCase(deleteCounterRepository)
    }

    @Test
    fun `should delete counter success`() {
        runBlockingTest {
            whenever(deleteCounterRepository.invoke(counterID)).thenReturn(successResponse)
            val result = deleteCounterUseCase(counterID)
            assertEquals(result,successResponse)
        }
    }

    @Test
    fun `should delete counter error`() {
        runBlockingTest {
            whenever(deleteCounterRepository.invoke(counterID)).thenReturn(errorResponse)
            val result = deleteCounterUseCase(counterID)
            assertEquals(result, errorResponse)
        }
    }

}
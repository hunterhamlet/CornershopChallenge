package com.cornershop.provider.usesCases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.GetCountersRepository
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.usecases.GetCountersUseCase
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class GetCountersUseCaseTest {
    @Mock
    lateinit var getCountersRepository: GetCountersRepository

    lateinit var getCountersUseCase: GetCountersUseCase

    private val listResponse: MutableList<CounterResponse> =
        mutableListOf(CounterResponse(id = "qwerty", title = "Cup of coffee", count = 1))
    private val successResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Success(data = listResponse)
    private val errorResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Failure(error = "Problems with server, try late.")

    @Before
    fun setup() {
        getCountersUseCase = GetCountersUseCase(getCountersRepository)
    }

    @Test
    fun `should get counter success`() {
        runBlockingTest {
            whenever(getCountersRepository.invoke()).thenReturn(successResponse)
            val result = getCountersUseCase()
            assertEquals(result,successResponse)
        }
    }

    @Test
    fun `should get counter error`() {
        runBlockingTest {
            whenever(getCountersRepository.invoke()).thenReturn(errorResponse)
            val result = getCountersUseCase()
            assertEquals(result, errorResponse)
        }
    }
}
package com.cornershop.provider.usesCases

import com.cornershop.provider.models.CounterResponse
import com.cornershop.provider.repository.remote.DecrementCounterRepository
import com.cornershop.provider.sealed.APIResult
import com.cornershop.provider.usecases.DecrementCounterUseCase
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class DecrementUseCaseTest {

    @Mock
    lateinit var decrementCounterRepository: DecrementCounterRepository

    lateinit var decrementCounterUseCase: DecrementCounterUseCase

    private val counterID: String = "qwerty"
    private val listResponse: MutableList<CounterResponse> =
        mutableListOf(CounterResponse(id = "qwerty", title = "Cup of coffee", count = 1))
    private val successResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Success(data = listResponse)
    private val errorResponse: APIResult<MutableList<CounterResponse>> =
        APIResult.Failure(error = "Problems with server, try late.")

    @Before
    fun setup() {
        decrementCounterUseCase = DecrementCounterUseCase(decrementCounterRepository)
    }

    @Test
    fun `should decrement counter success`() {
        runBlockingTest {
            whenever(decrementCounterRepository.invoke(counterID)).thenReturn(successResponse)
            val result = decrementCounterUseCase(counterID)
            assertEquals(result, successResponse)
        }
    }

    @Test
    fun `should decrement counter error`() {
        runBlockingTest {
            whenever(decrementCounterRepository.invoke(counterID)).thenReturn(errorResponse)
            val result = decrementCounterUseCase(counterID)
            assertEquals(result, errorResponse)
        }
    }
}